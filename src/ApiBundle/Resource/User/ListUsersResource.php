<?php

namespace ApiBundle\Resource\User;

use ApiBundle\Resource\ResourceInterface;
use AppBundle\Entity\User;

class ListUsersResource implements ResourceInterface
{
    /**
     * @var array
     */
    protected $users;

    /**
     * @param array $users
     */
    public function __construct(array $users)
    {
        $this->users = $users;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $users = array_map(function(User $user) {
            return (new SingleUserResource($user))->jsonSerialize();
        }, $this->users);

        return compact('users');
    }
}

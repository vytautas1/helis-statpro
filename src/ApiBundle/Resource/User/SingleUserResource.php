<?php

namespace ApiBundle\Resource\User;

use ApiBundle\Resource\ResourceInterface;
use AppBundle\Entity\User;

class SingleUserResource implements ResourceInterface
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->user->getId(),
            'email' => $this->user->getEmail(),
            'username' => $this->user->getUsername(),
            'createdAt' => $this->user->getCreatedAt()->getTimestamp(),
            'updatedAt' => $this->user->getUpdatedAt()->getTimestamp(),
        ];
    }
}

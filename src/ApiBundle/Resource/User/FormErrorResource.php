<?php

namespace ApiBundle\Resource\User;

use ApiBundle\Resource\ResourceInterface;

class FormErrorResource implements ResourceInterface
{
    /**
     * @var array
     */
    protected $errors;

    /**
     * @var string
     */
    private $title;

    /**
     * @param array $errors
     * @param string|null $title
     */
    public function __construct(array $errors, $title = "Form error")
    {
        $this->errors = $errors;
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'error' => [
                'code' => 400,
                'message' => $this->title,
                'errors' => $this->errors,
            ],
        ];
    }
}

<?php

namespace ApiBundle\EventListener;

use JsonSerializable;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Translation\TranslatorInterface;

class ApiResponseListener
{
    /**
     * @var string
     */
    private $environment;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param string $environment
     * @param TranslatorInterface $translator
     * @param LoggerInterface|null $logger
     */
    public function __construct($environment, TranslatorInterface $translator, LoggerInterface $logger = null)
    {
        $this->environment = $environment;
        $this->logger = $logger;
        $this->translator = $translator;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if (!$this->isApiRequest($request)) {
            return;
        }

        // request language
        if ($request->headers->has('Language')) {
            $this->translator->setLocale($request->headers->get('Language'));
            $request->setLocale($request->headers->get('Language'));
        }

        // request content type
        if ($type = $request->getContentType()) {
            switch ($type) {
                case 'json':
                    $request->setRequestFormat('json');
                    break;
                default:
                    $mime = $request->headers->get('Content-Type');
                    throw new HttpException(406,
                        "The content type: \"{$type}\" specified as mime \"{$mime}\" - is not supported.");
            }
        } else {
            // default format is JSON
            $request->setRequestFormat('json');
        }

        // request accept content type, currently only JSON
        $accepts = $request->getAcceptableContentTypes();
        $types = array_filter(array_unique(array_map([$request, 'getFormat'], $accepts)));

        if ($types && !in_array('json', $types, true)) {
            $acceptable = implode(',', $accepts);
            throw new HttpException(406, "None of acceptable content types: {$acceptable} are supported.");
        }

        // if there is a body, decode it currently as JSON only
        if ($content = $request->getContent()) {
            $data = @json_decode($content, true);

            if (null === $data) {
                // the error may be important, log it
                if (null !== $this->logger) {
                    $this->logger->error("Failed to parse json request content, err: " . json_last_error_msg());
                }

                throw new HttpException(400, "The given content is not a valid json.");
            }

            $request->request = new ParameterBag($data);
        }
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $request = $event->getRequest();

        if (!$this->isApiRequest($request)) {
            return;
        }

        $data = $event->getControllerResult();

        switch (true) {
            case is_array($data):
            case $data instanceof JsonSerializable:
                $response = new JsonResponse($data);
                break;
            case is_string($data):
                $response = new Response($data);
                $response->headers->set('Content-Type', 'application/json');
                break;
            default:
                throw new \UnexpectedValueException("Response type: " . gettype($data) . " from controller was not expected.");
        }

        $response->headers->set('Language', $this->translator->getLocale());
        $event->setResponse($response);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function isApiRequest(Request $request): bool
    {
        return strpos($request->getRequestUri(), '/api') === 0;
    }
}

<?php

namespace ApiBundle\Controller\Traits;

use ApiBundle\Resource\User\FormErrorResource;
use Symfony\Component\Form\Form;

trait FormErrorIteratorTrait
{
    /**
     * @param Form $form
     * @return FormErrorResource
     */
    private function getFormErrors(Form $form): FormErrorResource
    {
        $errors = [];

        foreach ($form->getErrors(true) as $error) {
            $fieldName = $error->getOrigin()->getName();
            $errors[$fieldName] = $error->getMessage();
        }

        $resourceTitle = $this->get('translator')->trans('error.form.validation', [], 'validators');

        return new FormErrorResource($errors, $resourceTitle);
    }
}

<?php

namespace ApiBundle\Controller;

use ApiBundle\Controller\Traits\FormErrorIteratorTrait;
use ApiBundle\Resource\User\ListUsersResource;
use ApiBundle\Resource\User\SingleUserResource;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/users")
 */
class UserController extends Controller
{
    use FormErrorIteratorTrait;

    /**
     * @Route("", name="api_users")
     * @Method("GET")
     *
     * @param EntityManagerInterface $em
     *
     * @return ListUsersResource
     */
    public function listAction(EntityManagerInterface $em): ListUsersResource
    {
        $users = $em->getRepository('AppBundle:User')->findAll();

        return new ListUsersResource($users);
    }

    /**
     * @Route("/me", name="api_user_me")
     * @Method("GET")
     *
     * @return SingleUserResource
     */
    public function meAction(): SingleUserResource
    {
        return new SingleUserResource($this->getUser());
    }

    /**
     * @Route("/{id}", name="api_user")
     * @Method("GET")
     *
     * @param User $user
     *
     * @return SingleUserResource
     */
    public function getAction(User $user): SingleUserResource
    {
        return new SingleUserResource($user);
    }

    /**
     * @Route("", name="api_user_post")
     * @Method("POST")
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     */
    public function createAction(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $form = $this->getForm(new User(), $request);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse($this->getFormErrors($form), Response::HTTP_BAD_REQUEST);
        }

        $user = $form->getData();

        $em->persist($user);
        $em->flush();

        $headers = [
            'Location' => $this->generateUrl('api_user', [
                'id' => $user->getId()
            ], UrlGeneratorInterface::ABSOLUTE_URL),
        ];

        return new JsonResponse(null, Response::HTTP_NO_CONTENT, $headers);
    }

    /**
     * @Route("/{id}", name="api_user_patch")
     * @Method("PATCH")
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @param Request $request
     * @param User $user
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     */
    public function updateAction(Request $request, User $user, EntityManagerInterface $em): JsonResponse
    {
        $form = $this->getForm($user, $request);
        $form->submit($request->request->get($form->getName()), false); // Don't clear missing values


        if (!$form->isValid()) {
            return new JsonResponse($this->getFormErrors($form), Response::HTTP_BAD_REQUEST);
        }

        $user = $form->getData();
        $em->persist($user);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/{id}", name="api_user_delete")
     * @Method("DELETE")
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @param User $user
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     */
    public function removeAction(User $user, EntityManagerInterface $em): JsonResponse
    {
        $em->remove($user);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param User $user
     * @param Request $request
     *
     * @return Form
     */
    private function getForm(User $user, Request $request): Form
    {
        return $this->createForm(UserType::class, $user, [
            'method' => $request->getMethod(),
            'csrf_protection' => false,
        ]);
    }
}

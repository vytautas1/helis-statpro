<?php

namespace ApiClientBundle\Form;

use ApiClientBundle\Entity\Setting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Setting $setting */
        $setting = $builder->getData();

        $type = $setting->isType(Setting::TYPE_BOOLEAN) ? CheckboxType::class : TextType::class;

        $builder
            ->add('value', $type, [
                'translation_domain' => 'ApiClientBundle',
                'label' => $setting->getTranslationLabel(),
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Setting::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'setting';
    }
}

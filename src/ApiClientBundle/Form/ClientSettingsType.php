<?php

namespace ApiClientBundle\Form;

use ApiClientBundle\Entity\Setting;
use ApiClientBundle\EventListener\AddSettingsListener;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientSettingsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Setting $setting */
        foreach ($builder->getData() as $setting) {
            $builder->add($setting->getAlias(), SettingType::class, [
                'translation_domain' => 'ApiClientBundle',
                'label' => false,
                'data' => $setting,
            ]);
        }

        $builder->addEventSubscriber(new AddSettingsListener());
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'client_setting';
    }
}

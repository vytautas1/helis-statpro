<?php

namespace ApiClientBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class ApiClientExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = $this->processConfiguration(new Configuration(), $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $definition = $container->getDefinition('ApiClientBundle\Service\StatProProvider');

        if ($configuration['endpoint_url']) {
            $definition->addMethodCall('setEndpointUrl', [$configuration['endpoint_url']]);
        }

        if ($configuration['enable']) {
            $definition->addMethodCall('setEnabled', [$configuration['enable']]);
        }
    }
}

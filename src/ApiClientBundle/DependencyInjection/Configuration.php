<?php

namespace ApiClientBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('api_client');

        $rootNode
            ->children()
                ->booleanNode('enable')
                    ->defaultNull()
                ->end()
                ->scalarNode('endpoint_url')
                    ->defaultNull()
                ->end()
            ->end();

        return $treeBuilder;
    }
}

<?php

namespace ApiClientBundle\Service;

use Psr\Log\LoggerInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;

class StatProProvider implements ProviderInterface
{
    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var string
     */
    private $endpointUrl;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     * @param Client $client
     */
    public function __construct(Client $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    public function getTournaments($page = 1, $limit = 10, $upcoming = 'true'): array
    {
        $params = $this->buildParams($page, $limit, true, $upcoming);
        $query = $this->buildUrl('tournaments', $params);

        return $this->getResponse($query);
    }

    public function getTournamentTeams($tournament, $page = 1, $limit = 10): array
    {
        $params = $this->buildParams($page, $limit);
        $query = $this->buildUrl(sprintf('tournament/%d/teams', $tournament), $params);

        return $this->getResponse($query);
    }

    public function getTeamPlayers($team, $page = 1, $limit = 100): array
    {
        $params = $this->buildParams($page, $limit, false);
        $query = $this->buildUrl(sprintf('team/%d/players', $team), $params);

        return $this->getResponse($query);
    }

    public function getPlayer($player): array
    {
        $query = $this->buildUrl(sprintf('player/%d', $player));

        return $this->getResponse($query);
    }

    public function getTeamStats($team): array
    {
        $statistics = [];
        $players = $this->getTeamPlayers($team);

        if (!empty($players)) {
            foreach ($players as $player) {
                $statistics[] = $this->getPlayer($player['id']);
            }
        }

        return $statistics;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @param $endpointUrl
     */
    public function setEndpointUrl($endpointUrl)
    {
        $this->endpointUrl = $endpointUrl;
    }

    /**
     * @return bool
     */
    protected function isEnabled(): bool
    {
        return (bool)$this->enabled;
    }

    /**
     * @return string
     */
    protected function getEndpointUrl(): string
    {
        return rtrim($this->endpointUrl, '/') . '/%s';
    }

    /**
     * @param int $page
     * @param int $limit
     * @param bool $meta
     * @param bool $upcoming
     *
     * @return array
     */
    private function buildParams($page, $limit, $meta = true, $upcoming = false): array
    {
        $params = array_merge(compact('page'), compact('limit'));

        if ($upcoming) {
            $starts_from = time();
            $params = array_merge($params, compact('upcoming'), compact('starts_from'));
        }

        if ($meta) {
            $params['with_meta'] = 'true';
        }

        return $params;
    }

    /**
     * @param string $action
     * @param array|null $params
     *
     * @return string
     * @throws \Exception
     */
    private function buildUrl($action, array $params = []): string
    {
        if (!$action) {
            throw new \Exception('Missing API action name');
        }

        $query = sprintf($this->getEndpointUrl(), $action);

        if (!empty($params)) {
            $query .= '?' . http_build_query($params);
        }

        return $query;
    }

    /**
     * @param string $url
     * @param string $method
     *
     * @return array
     */
    private function getResponse($url, $method = 'GET'): array
    {
        if ($this->isEnabled()) {
            try {
                $request = new Request($method, $url);
                $response = $this->client->send($request);

                return json_decode((string)$response->getBody(), true);
            } catch (\Exception $e) {
                if (!empty($this->logger)) {
                    $this->logger->error(sprintf('Encountered exception calling API - %s', $e->getMessage()));
                }
            }
        }

        return [];
    }
}

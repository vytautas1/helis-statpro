<?php

namespace ApiClientBundle\Service;

interface ProviderInterface
{
    /**
     * @param int $page
     * @param int $limit
     * @param bool $upcoming
     *
     * @return array
     */
    public function getTournaments($page, $limit, $upcoming);

    /**
     * @param int $tournament
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function getTournamentTeams($tournament, $page, $limit);

    /**
     * @param int $team
     *
     * @return array
     */
    public function getTeamPlayers($team);

    /**
     * @param int $team
     *
     * @return array
     */
    public function getTeamStats($team);

    /**
     * @param int $player
     *
     * @return array
     */
    public function getPlayer($player);
}

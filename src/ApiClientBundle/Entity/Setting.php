<?php

namespace ApiClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="ApiClientBundle\Repository\SettingRepository")
 * @ORM\Table(name="settings")
 * @UniqueEntity(fields="alias", message="setting.alias.exist")
 */
class Setting
{
    const TRANSLATION_PREFIX = 'setting.label.';

    const TYPE_STRING = 'string';
    const TYPE_BOOLEAN = 'bool';

    const ALIAS_CLIENT_ENABLE = 'client_enable';
    const ALIAS_CLIENT_ENDPOINT_URL = 'client_endpoint_url';

    /**
     * @return array
     */
    public static $getAliases = [
        self::ALIAS_CLIENT_ENABLE => self::TYPE_BOOLEAN,
        self::ALIAS_CLIENT_ENDPOINT_URL => self::TYPE_STRING,
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(length=15)
     * @Assert\NotBlank(message="setting.type.not_blank")
     * @Assert\Length(max="15")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\NotBlank(message="setting.alias.not_blank")
     * @Assert\Length(max="25")
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $value;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param string $type
     * @return bool
     */
    public function isType($type): bool
    {
        return $this->type == $type;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return string|null
     */
    public function getValue()
    {
        return $this->isType(self::TYPE_BOOLEAN) ? (bool) $this->value : $this->value;
    }

    /**
     * @param string|null $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getTranslationLabel(): string
    {
        return self::TRANSLATION_PREFIX . $this->getAlias();
    }
}

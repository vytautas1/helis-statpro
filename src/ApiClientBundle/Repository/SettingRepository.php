<?php

namespace ApiClientBundle\Repository;

use ApiClientBundle\Entity\Setting;
use Doctrine\ORM\EntityRepository;

class SettingRepository extends EntityRepository
{
    /**
     * @return string|null
     */
    public function getEndpointUrl(): ?string
    {
        $setting = $this->findOneBy([
            'alias' => Setting::ALIAS_CLIENT_ENDPOINT_URL,
        ]);

        return $setting ? $setting->getValue() : null;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        $setting = $this->findOneBy([
            'alias' => Setting::ALIAS_CLIENT_ENABLE,
        ]);

        return $setting ? (bool)$setting->getValue() : false;
    }
}

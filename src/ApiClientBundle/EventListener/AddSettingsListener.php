<?php

namespace ApiClientBundle\EventListener;

use ApiClientBundle\Entity\Setting;
use ApiClientBundle\Form\SettingType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AddSettingsListener implements EventSubscriberInterface
{
    const TRANSLATION_DOMAIN = 'ApiClientBundle';

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        $form = $event->getForm();

        foreach (Setting::$getAliases as $alias => $type) {
            if (!$form->has($alias)) {
                $setting = new Setting();
                $setting->setAlias($alias);
                $setting->setType($type);

                $form->add($alias, SettingType::class, [
                    'translation_domain' => self::TRANSLATION_DOMAIN,
                    'label' => false,
                    'data' => $setting,
                ]);
            }
        }
    }
}

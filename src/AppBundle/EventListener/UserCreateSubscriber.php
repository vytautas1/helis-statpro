<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserCreateSubscriber implements EventSubscriberInterface
{
    /**
     * @var array
     */
    private $options;

    /**
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::SUBMIT => 'submit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $user = $event->getData();
        $form = $event->getForm();

        if ((!$user || null === $user->getId()) && !$this->options['csrf_protection']) {
            $form->add('password', PasswordType::class, [
                'mapped' => false,
                'constraints' => [
                    new NotBlank()
                ]
            ]);
        }
    }

    /**
     * @param FormEvent $event
     */
    public function submit(FormEvent $event)
    {
        $user = $event->getData();
        $form = $event->getForm();

        if ($user instanceof User && $form->has('password') && !$this->options['csrf_protection']) {
            $user->setPlainPassword($form->get('password')->getData());
        }
    }
}

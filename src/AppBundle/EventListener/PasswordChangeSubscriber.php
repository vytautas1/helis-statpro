<?php

namespace AppBundle\EventListener;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class PasswordChangeSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TranslatorInterface $translator
     * @internal param TokenStorage $tokenStorage
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator) {
        $this->passwordEncoder = $passwordEncoder;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::SUBMIT => 'submit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function submit(FormEvent $event)
    {
        $form = $event->getForm();
        $currentPasswordInput = $form->get('currentPassword');

        if (!$this->passwordEncoder->isPasswordValid($form->getData(), $currentPasswordInput->getData())) {
            $currentPasswordInput->addError(new FormError($this->translator->trans('error.password.current.invalid')));
        }
    }
}

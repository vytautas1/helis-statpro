<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    const DOMAIN = 'statpro.dev';
    const PASSWORD = 'S3cretpassword';

    /**
     * @var Container
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $users = [
            'mario' => ['ROLE_USER'],
            'luigi' => ['ROLE_ADMIN'],
        ];

        foreach ($users as $username => $roles) {
            $user = new User();
            $user->setUsername(ucfirst($username));
            $user->setEmail($username . '@' . self::DOMAIN);
            $user->setRoles($roles);
            $user->setPlainPassword(self::PASSWORD);

            $manager->persist($user);

            $this->setReference('user-' . $username, $user);
        }

        $manager->flush();
    }
}

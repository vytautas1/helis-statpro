<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Validator\Constraints\Email;

class AppUserCreateCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:user:create')
            ->setDescription('Create a user')
            ->setDefinition([
                new InputArgument('username', InputArgument::REQUIRED, 'The username'),
                new InputArgument('email', InputArgument::REQUIRED, 'The email'),
                new InputArgument('password', InputArgument::REQUIRED, 'The password'),
                new InputOption('admin', null, InputOption::VALUE_NONE, 'Set the user role to admin'),
            ])
            ->setHelp(<<<'EOT'
The <info>app:user:create</info> command creates a user:
  <info>php %command.full_name% Gordy</info>
This interactive shell will ask you for an email and then a password.
You can alternatively specify the email and password as the second and third arguments:
  <info>php %command.full_name% Gordy gordon.freeman@half-life.com BlueShift</info>
You can create a user with admin role using the admin flag:
  <info>php %command.full_name% --admin</info>
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $admin = $input->getOption('admin');

        try {
            $this->createUser($username, $email, $password, $admin);
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Error occurred while trying to create user. Please try again.</error>'));
            return;
        }

        $output->writeln(sprintf('<info>%s is created successfully!</info>', $admin ? 'Admin user': 'User'));
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];

        if (!$input->getArgument('username')) {
            $question = new Question('Please choose a username: ');
            $question->setValidator(function ($username) {
                if (empty($username)) {
                    throw new \Exception('Username can not be empty');
                }
                return $username;
            });
            $questions['username'] = $question;
        }

        if (!$input->getArgument('email')) {
            $validator = $this->getContainer()->get('validator');

            $question = new Question('Please choose an email: ');
            $question->setValidator(function ($email) use ($validator) {
                if (empty($email)) {
                    throw new \Exception('Email can not be empty');
                }
                if ($validator->validate($email, [new Email()])->count()) {
                    throw new \Exception('Invalid email provided');
                }
                return $email;
            });
            $questions['email'] = $question;
        }

        if (!$input->getArgument('password')) {
            $question = new Question('Please choose a password: ');
            $question->setValidator(function ($password) {
                if (empty($password)) {
                    throw new \Exception('Password can not be empty');
                }
                return $password;
            });
            $question->setHidden(true);
            $questions['password'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     * @param bool $admin
     */
    protected function createUser($username, $email, $password, $admin)
    {
        $passwordEncoder = $this->getContainer()->get('security.password_encoder');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $user = new User();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPassword($passwordEncoder->encodePassword($user, $password));
        $user->setRoles($admin ? ['ROLE_ADMIN'] : ['ROLE_USER']);

        $em->persist($user);
        $em->flush();
    }
}

<?php

namespace AppBundle\Controller;

use ApiClientBundle\Form\ClientSettingsType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class SettingsController extends Controller
{
    /**
     * @Route("/settings", name="client_settings")
     * @Method({"GET", "POST"})
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     *
     * @return Response
     */
    public function indexAction(Request $request, EntityManagerInterface $em, TranslatorInterface $translator): Response
    {
        $settings = $this->getDoctrine()->getRepository('ApiClientBundle:Setting')->findAll();

        $form = $this->createForm(ClientSettingsType::class, $settings);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($form->getData() as $setting) {
                $em->persist($setting);
            }

            $em->flush();
            $this->addFlash('success', $translator->trans('flash.success.settings'));

            $this->redirectToRoute('client_settings');
        }

        return $this->render('@ApiClient/settings.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

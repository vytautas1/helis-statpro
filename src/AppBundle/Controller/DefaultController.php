<?php

namespace AppBundle\Controller;

use ApiClientBundle\Service\StatProProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render(':default:index.html.twig');
    }

    /**
     * @Route("/tournaments", name="tournaments")
     * @Method("GET")
     *
     * @param Request $request
     * @param AuthorizationCheckerInterface $auth
     * @param StatProProvider $provider
     *
     * @return Response
     */
    public function getTournaments(Request $request, AuthorizationCheckerInterface $auth, StatProProvider $provider): Response
    {
        $page = $auth->isGranted('IS_AUTHENTICATED_FULLY') ? $request->query->getInt('page', 1) : null;

        return $this->render(':default:tournaments.html.twig', [
            'tournaments' => $provider->getTournaments($page),
        ]);
    }

    /**
     * @Route("/tournament/{id}/teams", name="tournament_teams", requirements={"id" = "\d+"})
     * @Method("GET")
     *
     * @param Request $request
     * @param StatProProvider $provider
     * @param int $id
     *
     * @return Response
     */
    public function getTournamentTeamsAction(Request $request, StatProProvider $provider, $id): Response
    {
        $page = $request->query->getInt('page', 1);

        return $this->render(':default:tournament_teams.html.twig', [
            'teams' => $provider->getTournamentTeams($id, $page),
        ]);
    }

    /**
     * @Route("/team/{id}/stats", name="team_statistics", requirements={"id" = "\d+"})
     * @Method("GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     *
     * @param StatProProvider $provider
     * @param int $id
     *
     * @return Response
     */
    public function getTeamStatisticAction(StatProProvider $provider, $id): Response
    {
        return $this->render(':default:team_statistics.html.twig', [
            'statistics' => $provider->getTeamStats($id),
        ]);
    }
}

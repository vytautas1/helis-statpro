<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class SettingsControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var ReferenceRepository
     */
    private $fixtures;

    public function setUp()
    {
        $this->client = $this->makeClient();

        $this->fixtures = $this->loadFixtures([
            'AppBundle\DataFixtures\ORM\LoadUserData',
        ])->getReferenceRepository();
    }

    public function testNonAdminUserShouldNotAccessSettingsPage()
    {
        /** @var User $user */
        $user = $this->fixtures->getReference('user-mario');
        $this->loginAs($user, 'main');

        $this->client = $this->makeClient();
        $this->client->request('GET', '/settings');

        $this->assertStatusCode(403, $this->client);
    }

    public function testCanUpdateSettings()
    {
        /** @var User $user */
        $user = $this->fixtures->getReference('user-luigi');
        $this->loginAs($user, 'main');

        $this->client = $this->makeClient();
        $this->client->followRedirects();

        $crawler = $this->client->request('GET', '/settings');

        $form = $crawler->selectButton('update')->form();
        $form->setValues([
            'client_setting[client_enable][value]' => true,
            'client_setting[client_endpoint_url][value]' => 'test-value',
        ]);
        $this->client->submit($form);

        $response = $this->client->getResponse();

        $this->isSuccessful($response);
        $this->assertContains('Settings has been changed!', $response->getContent());
    }
}

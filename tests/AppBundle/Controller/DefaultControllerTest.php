<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class DefaultControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var ReferenceRepository
     */
    private $fixtures;

    public function setUp()
    {
        $this->client = $this->makeClient();

        $this->fixtures = $this->loadFixtures([
            'AppBundle\DataFixtures\ORM\LoadUserData',
        ])->getReferenceRepository();
    }

    public function testIndex()
    {
        $crawler = $this->client->request('GET','/');

        $this->isSuccessful($this->client->getResponse());
        $this->assertContains('A Warm Welcome!', $crawler->filter('#content h1')->text());
    }

    public function testGetTournaments()
    {
        $crawler = $this->client->request('GET', '/tournaments');

        $this->isSuccessful($this->client->getResponse());
        $this->assertContains('Tournaments', $crawler->filter('#content h1')->text());
    }

    public function testAnonymousUserShouldNotSeePaginator()
    {
        $crawler = $this->client->request('GET', '/tournaments');

        $this->isSuccessful($this->client->getResponse());
        $this->assertEmpty($crawler->filter('ul.pagination'));
    }

    public function testLoggedInUserShouldSeeTournamentPaginator()
    {
        /** @var User $user */
        $user = $this->fixtures->getReference('user-mario');
        $this->loginAs($user, 'main');

        $this->client = $this->makeClient();
        $crawler = $this->client->request('GET', '/tournaments');

        $this->isSuccessful($this->client->getResponse());
        $this->assertCount(1, $crawler->filter('ul.pagination'));
    }

    public function testLoggedInUserCanAccessTournamentPage()
    {
        /** @var User $user */
        $user = $this->fixtures->getReference('user-mario');
        $this->loginAs($user, 'main');

        $this->client = $this->makeClient();
        $this->client->request('GET', '/tournaments?page=2');

        $this->isSuccessful($this->client->getResponse());
    }

    public function testAnonymousUserShouldSeeSameTournaments()
    {
        $crawler = $this->client->request('GET', '/tournaments');
        $page = $crawler->html();

        $crawler = $this->client->request('GET', '/tournaments?page=2');

        $this->assertEquals($crawler->html(), $page);
    }

    public function testLoggedInUserCanAccessTeamList()
    {
        /** @var User $user */
        $user = $this->fixtures->getReference('user-mario');
        $this->loginAs($user, 'main');

        $this->client = $this->makeClient();
        $crawler = $this->client->request('GET', '/tournaments');
        $uri = $crawler->filter('table > tbody > tr:first-child > td:last-child > a')->attr('href');

        $crawler = $this->client->request('GET', $uri);

        $this->isSuccessful($this->client->getResponse());
        $this->assertContains('Tournament Teams', $crawler->filter('#content h1')->text());
    }

    public function testAnonymousUserCanAccessTeamList()
    {
        $crawler = $this->client->request('GET', '/tournaments');
        $uri = $crawler->filter('table > tbody > tr:first-child > td:last-child > a')->attr('href');

        $crawler = $this->client->request('GET', $uri);

        $this->isSuccessful($this->client->getResponse());
        $this->assertContains('Tournament Teams', $crawler->filter('#content h1')->text());
    }

    public function testLoggedInUserCanAccessTeamStatisticsPage()
    {
        /** @var User $user */
        $user = $this->fixtures->getReference('user-mario');
        $this->loginAs($user, 'main');

        $this->client = $this->makeClient();
        $crawler = $this->client->request('GET', '/tournaments');

        $teamUri = $crawler->filter('table > tbody > tr:first-child > td:last-child > a')->attr('href');
        $crawler = $this->client->request('GET', $teamUri);

        $teamStatsUri = $crawler->filter('table > tbody > tr:first-child > td:nth-child(2) > a')->attr('href');
        $crawler = $this->client->request('GET', $teamStatsUri);

        $this->isSuccessful($this->client->getResponse());
        $this->assertContains('Team Statistics', $crawler->filter('#content h1')->text());
    }
}

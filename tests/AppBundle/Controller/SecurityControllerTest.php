<?php

namespace Tests\AppBundle\Controller;

use AppBundle\DataFixtures\ORM\LoadUserData;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class SecurityControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var ReferenceRepository
     */
    private $fixtures;

    /**
     * @var array
     */
    private static $user = [
        'email' => 'duke.nukem@pieceofcake.com',
        'username' => 'DukeNukem',
        'password' => 'holyCow!',
    ];

    public function setUp()
    {
        $this->client = $this->makeClient();

        $this->fixtures = $this->loadFixtures([
            'AppBundle\DataFixtures\ORM\LoadUserData',
        ])->getReferenceRepository();
    }

    public function testUserCanLogin()
    {
        /** @var User $user */
        $user = $this->fixtures->getReference('user-mario');

        $crawler = $this->client->request('GET', '/login');

        $form = $crawler->selectButton('Login')->form();
        $form->setValues([
            '_email' => $user->getEmail(),
            '_password' => LoadUserData::PASSWORD,
        ]);

        $this->client->submit($form);
        $crawler = $this->client->request('GET', '/');

        $this->isSuccessful($this->client->getResponse());
        $this->assertContains(ucfirst($user->getUsername()), $crawler->filter('.dropdown .user-name')->text());
    }

    public function testUserCanChangePassword()
    {
        $newPassword = 'iLoveMushrooms';

        /** @var User $user */
        $user = $this->fixtures->getReference('user-mario');

        $this->loginAs($user, 'main');
        $this->client = $this->makeClient();
        $this->client->followRedirects();

        $crawler = $this->client->request('GET', '/change-password');

        $form = $crawler->selectButton('change-password')->form();
        $form->setValues([
            'user_change_password[currentPassword]' => LoadUserData::PASSWORD,
            'user_change_password[plainPassword][first]' => $newPassword,
            'user_change_password[plainPassword][second]' => $newPassword,
        ]);
        $this->client->submit($form);

        $response = $this->client->getResponse();

        $this->isSuccessful($response);
        $this->assertContains('Password has been changed!', $response->getContent());
    }

    public function testUserCanSignUp()
    {
        $crawler = $this->client->request('GET', '/signup');

        $form = $crawler->selectButton('signup')->form();
        $form->setValues([
            'user[email]' => self::$user['email'],
            'user[username]' => self::$user['username'],
            'user[plainPassword][first]' => self::$user['password'],
            'user[plainPassword][second]' => self::$user['password'],
        ]);

        $this->client->submit($form);
        $crawler = $this->client->request('GET', '/');

        $this->isSuccessful($this->client->getResponse());
        $this->assertContains(self::$user['username'], $crawler->filter('.dropdown .user-name')->text());
    }

    public function testUseCanLogout()
    {
        $this->client->request('GET', '/logout');

        $this->assertTrue($this->client->getResponse()->isRedirect('http://localhost/login'));
    }
}

<?php

namespace Tests\AppBundle\EventListener;

use AppBundle\Entity\User;
use AppBundle\EventListener\AutoLoginListener;
use Doctrine\ORM\Event\LifecycleEventArgs;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AutoLoginListenerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var TokenStorageInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $tokenStorage;

    /**
     * @var LifecycleEventArgs|PHPUnit_Framework_MockObject_MockObject
     */
    private $eventArgs;

    public function setUp()
    {
        $this->tokenStorage = $this->getMockBuilder(TokenStorageInterface::class)->getMock();
        $this->eventArgs = $this->getMockBuilder(LifecycleEventArgs::class)->disableOriginalConstructor()
            ->getMock();
    }

    public function testSetTokenForLoggedInUser()
    {
        $this->eventArgs->expects($this->once())
            ->method('getEntity')
            ->willReturn(new User());

        $this->tokenStorage->expects($this->once())
            ->method('setToken');

        $listener = new AutoLoginListener($this->tokenStorage);
        $listener->postPersist($this->eventArgs);
    }

    public function testSkipIfUserIsNotSet()
    {
        $this->eventArgs->expects($this->once())
        ->method('getEntity')
        ->willReturn(null);

        $this->tokenStorage->expects($this->never())
            ->method('setToken');

        $listener = new AutoLoginListener($this->tokenStorage);
        $listener->postPersist($this->eventArgs);
    }
}

<?php

namespace Tests\AppBundle\EventListener;

use AppBundle\Entity\User;
use AppBundle\EventListener\PasswordChangeSubscriber;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class PasswordChangeSubscriberTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var UserPasswordEncoderInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $encoder;

    /**
     * @var TranslatorInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $translator;

    /**
     * @var FormEvent|PHPUnit_Framework_MockObject_MockObject
     */
    private $formEvent;

    /**
     * @var Form|PHPUnit_Framework_MockObject_MockObject
     */
    private $form;

    /**
     * @var Form|PHPUnit_Framework_MockObject_MockObject
     */
    private $input;

    public function setUp()
    {
        $this->encoder = $this->getMockBuilder(UserPasswordEncoderInterface::class)
            ->disableOriginalConstructor()->getMock();
        $this->translator = $this->getMockBuilder(TranslatorInterface::class)
            ->disableOriginalConstructor()->getMock();
        $this->formEvent = $this->getMockBuilder(FormEvent::class)->disableOriginalConstructor()->getMock();
        $this->form = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
        $this->input = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
    }

    public function testAddValidationErrorToCurrentPasswordField()
    {
        $this->input->expects($this->once())
            ->method('addError');

        $this->form->expects($this->once())
            ->method('get')
            ->with('currentPassword')
            ->willReturn($this->input);

        $this->form->expects($this->once())
            ->method('getData')
            ->willReturn(new User());

        $this->formEvent->expects($this->once())
            ->method('getForm')
            ->willReturn($this->form);

        $this->encoder->expects($this->once())
            ->method('isPasswordValid')
            ->willReturn(false);

        $subscriber = new PasswordChangeSubscriber($this->encoder, $this->translator);
        $subscriber->submit($this->formEvent);
    }

    public function testNoValidationErrorUponValidCurrentPassword()
    {
        $this->input->expects($this->never())
            ->method('addError');

        $this->form->expects($this->once())
            ->method('get')
            ->with('currentPassword')
            ->willReturn($this->input);

        $this->form->expects($this->once())
            ->method('getData')
            ->willReturn(new User());

        $this->formEvent->expects($this->once())
            ->method('getForm')
            ->willReturn($this->form);

        $this->encoder->expects($this->once())
            ->method('isPasswordValid')
            ->willReturn(true);

        $subscriber = new PasswordChangeSubscriber($this->encoder, $this->translator);
        $subscriber->submit($this->formEvent);
    }
}

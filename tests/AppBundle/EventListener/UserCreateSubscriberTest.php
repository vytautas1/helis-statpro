<?php

namespace Tests\AppBundle\EventListener;

use AppBundle\Entity\User;
use AppBundle\EventListener\UserCreateSubscriber;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormEvent;

class UserCreateSubscriberTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var array
     */
    private $options = [
        'csrf_protection' => false,
    ];

    /**
     * @var FormEvent|PHPUnit_Framework_MockObject_MockObject
     */
    private $formEvent;

    /**
     * @var Form|PHPUnit_Framework_MockObject_MockObject
     */
    private $form;

    /**
     * @var Form|PHPUnit_Framework_MockObject_MockObject
     */
    private $input;

    public function setUp()
    {
        $this->formEvent = $this->getMockBuilder(FormEvent::class)->disableOriginalConstructor()->getMock();
        $this->form = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
        $this->input = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
    }

    public function testAddPasswordField()
    {
        $this->formEvent->expects($this->once())
            ->method('getData')
            ->willReturn(null);

        $this->form->expects($this->once())
            ->method('add')
            ->with('password', PasswordType::class);

        $this->formEvent->expects($this->once())
            ->method('getForm')
            ->willReturn($this->form);

        $subscriber = new UserCreateSubscriber($this->options);
        $subscriber->preSetData($this->formEvent);
    }

    public function testSkipPasswordFieldIfCsrfProtectionIsEnabled()
    {
        $this->options['csrf_protection'] = true;

        $this->formEvent->expects($this->once())
            ->method('getData')
            ->willReturn(null);

        $this->form->expects($this->never())
            ->method('add');

        $this->formEvent->expects($this->once())
            ->method('getForm')
            ->willReturn($this->form);

        $subscriber = new UserCreateSubscriber($this->options);
        $subscriber->preSetData($this->formEvent);
    }

    public function testSetValueOfPasswordFieldAsPlainPassword()
    {
        $user = new User();

        $this->formEvent->expects($this->once())
            ->method('getData')
            ->willReturn($user);

        $this->form->expects($this->once())
            ->method('has')
            ->with('password')
            ->willReturn(true);

        $this->input->expects($this->once())
            ->method('getData')
            ->willReturn('demo-password');

        $this->form->expects($this->once())
            ->method('get')
            ->with('password')
            ->willReturn($this->input);

        $this->formEvent->expects($this->once())
            ->method('getForm')
            ->willReturn($this->form);

        $subscriber = new UserCreateSubscriber($this->options);
        $subscriber->submit($this->formEvent);

        $this->assertEquals('demo-password', $user->getPlainPassword());
    }

    public function testSkipIfCsrfProtectionIsEnabled()
    {
        $this->options['csrf_protection'] = true;

        $user = new User();

        $this->formEvent->expects($this->once())
            ->method('getData')
            ->willReturn($user);

        $this->form->expects($this->once())
            ->method('has')
            ->with('password')
            ->willReturn(true);

        $this->form->expects($this->never())
            ->method('get')
            ->with('password');

        $this->formEvent->expects($this->once())
            ->method('getForm')
            ->willReturn($this->form);

        $subscriber = new UserCreateSubscriber($this->options);
        $subscriber->submit($this->formEvent);
    }
}

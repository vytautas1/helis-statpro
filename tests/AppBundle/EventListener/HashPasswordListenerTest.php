<?php

namespace Tests\AppBundle\EventListener;

use AppBundle\Entity\User;
use AppBundle\EventListener\HashPasswordListener;
use Doctrine\ORM\Event\LifecycleEventArgs;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HashPasswordListenerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var UserPasswordEncoderInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $encoder;

    /**
     * @var LifecycleEventArgs|PHPUnit_Framework_MockObject_MockObject
     */
    private $eventArgs;

    public function setUp()
    {
        $this->encoder = $this->getMockBuilder(UserPasswordEncoderInterface::class)
            ->disableOriginalConstructor()->getMock();
        $this->eventArgs = $this->getMockBuilder(LifecycleEventArgs::class)->disableOriginalConstructor()
            ->getMock();
    }

    public function testSkipSaveIfEventEntityIsNotUser()
    {
        $this->eventArgs->expects($this->once())
            ->method('getEntity')
            ->willReturn(null);

        $this->encoder->expects($this->never())
            ->method('encodePassword');

        $listener = new HashPasswordListener($this->encoder);
        $listener->prePersist($this->eventArgs);
    }

    public function testSkipUpdateIfEventEntityIsNotUser()
    {
        $this->eventArgs->expects($this->once())
            ->method('getEntity')
            ->willReturn(null);

        $this->encoder->expects($this->never())
            ->method('encodePassword');

        $listener = new HashPasswordListener($this->encoder);
        $listener->preUpdate($this->eventArgs);
    }

    public function testSkipOnEmptyPlainPassword()
    {
        $this->eventArgs->expects($this->once())
            ->method('getEntity')
            ->willReturn(new User());

        $this->encoder->expects($this->never())
            ->method('encodePassword');

        $listener = new HashPasswordListener($this->encoder);
        $listener->preUpdate($this->eventArgs);
    }

    public function testSetHashedPasswordForNewUser()
    {
        $user = new User();
        $user->setPlainPassword('demo-password');

        $this->eventArgs->expects($this->once())
            ->method('getEntity')
            ->willReturn($user);

        $this->encoder->expects($this->once())
            ->method('encodePassword')
            ->willReturn('hashed-password');

        $listener = new HashPasswordListener($this->encoder);
        $listener->prePersist($this->eventArgs);

        $this->assertEquals($user->getPassword(), 'hashed-password');
    }

    public function testUpdatedPasswordForExistingUser()
    {
        $user = new User();
        $user->setPlainPassword('demo-password');

        $this->eventArgs->expects($this->once())
            ->method('getEntity')
            ->willReturn($user);

        $this->encoder->expects($this->once())
            ->method('encodePassword')
            ->willReturn('hashed-password');

        $listener = new HashPasswordListener($this->encoder);
        $listener->preUpdate($this->eventArgs);

        $this->assertEquals('hashed-password', $user->getPassword());
    }
}

<?php

namespace Tests\AppBundle\Command;

use AppBundle\Command\AppUserChangePasswordCommand;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class AppUserChangePasswordCommandTest extends WebTestCase
{
    const NEW_PASSWORD = 'rescuePrincessPeach';

    /**
     * @var ReferenceRepository
     */
    private $fixtures;

    public function setUp()
    {
        $this->fixtures = $this->loadFixtures([
            'AppBundle\DataFixtures\ORM\LoadUserData',
        ])->getReferenceRepository();
    }

    public function testChangePasswordCommand()
    {
        $user = $this->fixtures->getReference('user-luigi');

        $kernel = static::createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->add(new AppUserChangePasswordCommand());

        $command = $application->find('app:user:change-password');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'email' => $user->getEmail(),
            'password' => self::NEW_PASSWORD,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains(sprintf('Changed password for user %s', $user->getEmail()), $output);
    }

    public function testUnableToChangePasswordForNonExistingUser()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->add(new AppUserChangePasswordCommand());

        $command = $application->find('app:user:change-password');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'email' => 'user@non-existing.com',
            'password' => self::NEW_PASSWORD,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('Unable to find user by provided email!', $output);
    }
}

<?php

namespace Tests\AppBundle\Command;

use AppBundle\Command\AppUserCreateCommand;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class AppUserCreateCommandTest extends WebTestCase
{
    /**
     * @var array
     */
    private static $user = [
        'email' => 'max.payne@paybacktime.com',
        'username' => 'MaxPayne',
        'password' => 'haveNoFearVladIsHere',
    ];

    public function setUp()
    {
        $this->loadFixtures([]);
    }

    public function testCreateUserCommand()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->add(new AppUserCreateCommand());

        $command = $application->find('app:user:create');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => self::$user['username'],
            'email' => self::$user['email'],
            'password' => self::$user['password'],
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('User is created successfully!', $output);
    }

    public function testCreateAdminUserCommand()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->add(new AppUserCreateCommand());

        $command = $application->find('app:user:create');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => self::$user['username'],
            'email' => self::$user['email'],
            'password' => self::$user['password'],
            '--admin' => null,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('Admin user is created successfully!', $output);
    }
}

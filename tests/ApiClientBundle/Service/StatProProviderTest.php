<?php

namespace Tests\ApiClientBundle\Service;

use ApiClientBundle\Service\StatProProvider;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;
use Tests\ApiClientBundle\RequestTestCase;

class StatProProviderTest extends RequestTestCase
{
    const ENDPOINT_URL = 'http://join.helis.lt/api';
    const TEAM_ID = 15;
    const PLAYER_ID = 7;

    /**
     * @var LoggerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $logger;

    /**
     * @var StatProProvider
     */
    protected $provider;

    /**
     * @var array
     */
    protected $mockData = [
        'results' => [
            'id' => 131,
            'name' => 'Bailey-Hickle Tournament',
            'starts_at' => '2017-07-03T12:31:31+0000',
            'ends_at' => '2017-07-05T12:31:31+0000',
            'teams' => [
                'id' => 230,
                'name' => 'Mauritania Lang, Veum and Hilll',
                'created_at' => '2017-05-02T09:16:42+0000',
            ],
        ],
        'limit' => 10,
        'page' => 1,
        'pages' => 5,
    ];

    public function setUp()
    {
        $responses = [
            new Response(200, ['content-type' => 'application/json'], json_encode($this->mockData)),
            new Response(200, ['content-type' => 'application/json'], json_encode([])),
        ];

        $handler = new MockHandler($responses);
        $history = Middleware::history($this->container);

        $stack = HandlerStack::create($handler);
        $stack->push($history);

        $this->client = new Client(['handler' => $stack]);
        $this->logger = $this->getMockBuilder(LoggerInterface::class)->disableOriginalConstructor()->getMock();

        $this->provider = new StatProProvider($this->client, $this->logger);
        $this->provider->setEnabled(true);
        $this->provider->setEndpointUrl(self::ENDPOINT_URL);
    }

    public function testGetTournamentsRequest()
    {
        $this->provider->getTournaments();
        $request = $this->container[0]['request'];

        $this->assertRequest($request, '/api/tournaments');
        $this->assertNotEmpty($this->getQueryParameter('upcoming'));
        $this->assertNotEmpty($this->getQueryParameter('with_meta'));
    }

    public function testGetTournamentTeams()
    {
        $this->provider->getTournamentTeams(self::TEAM_ID);
        $request = $this->container[0]['request'];

        $this->assertRequest($request, sprintf('/api/tournament/%d/teams', self::TEAM_ID));
        $this->assertNotEmpty($this->getQueryParameter('with_meta'));
    }

    public function testGetTeamPlayer()
    {
        $this->provider->getTeamPlayers(self::TEAM_ID);
        $request = $this->container[0]['request'];

        $this->assertRequest($request, sprintf('/api/team/%d/players', self::TEAM_ID));
    }

    public function testGetPlayer()
    {
        $this->provider->getPlayer(self::PLAYER_ID);
        $request = $this->container[0]['request'];

        $this->assertRequest($request, sprintf('/api/player/%d', self::PLAYER_ID));
    }

    public function testGetTeamStats()
    {
        $this->provider->getTeamStats(self::TEAM_ID);
        $request = $this->container[0]['request'];

        $this->assertRequest($request, sprintf('/api/team/%d/players', self::TEAM_ID));
    }

    public function testEmptyResultFromProvider()
    {
        $this->provider->getTeamPlayers(self::TEAM_ID);

        $this->assertEmpty($this->provider->getPlayer(self::PLAYER_ID));
    }

    public function testNoRequestIsSentIfProviderIsDisabled()
    {
        $this->provider->setEnabled(false);

        $this->assertEmpty($this->container);
    }

    public function testEmptyResultIfProviderIsDisabled()
    {
        $this->provider->setEnabled(false);

        $this->assertEmpty($this->provider->getTournaments());
    }

    /**
     * @param Request $request
     * @param string $path
     */
    private function assertRequest(Request $request, $path)
    {
        $this->assertEquals(parse_url(self::ENDPOINT_URL, PHP_URL_HOST), $request->getUri()->getHost());
        $this->assertEquals($path, $request->getUri()->getPath());
        $this->assertEquals(200, $this->getStatusCode());
    }
}

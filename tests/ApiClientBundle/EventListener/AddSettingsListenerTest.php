<?php

namespace Tests\ApiClientBundle\EventListener;

use ApiClientBundle\Entity\Setting;
use ApiClientBundle\EventListener\AddSettingsListener;
use ApiClientBundle\Form\SettingType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormEvent;

class AddSettingsListenerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var FormEvent|\PHPUnit_Framework_MockObject_MockObject
     */
    private $formEvent;

    /**
     * @var Form|\PHPUnit_Framework_MockObject_MockObject
     */
    private $form;

    public function setUp()
    {
        $this->formEvent = $this->getMockBuilder(FormEvent::class)->disableOriginalConstructor()->getMock();
        $this->form = $this->getMockBuilder(Form::class)->disableOriginalConstructor()->getMock();
    }

    public function testAddClientEndpointUrlSetting()
    {
        $this->formEvent->expects($this->once())
            ->method('getForm')
            ->willReturn($this->form);

        $this->form->expects($this->exactly(2))
            ->method('has')
            ->withConsecutive([Setting::ALIAS_CLIENT_ENABLE], [Setting::ALIAS_CLIENT_ENDPOINT_URL])
            ->willReturnOnConsecutiveCalls(true, false);

        $this->form->expects($this->once())
            ->method('add')
            ->with(Setting::ALIAS_CLIENT_ENDPOINT_URL, SettingType::class);

        $listener = new AddSettingsListener();
        $listener->onPreSetData($this->formEvent);
    }

    public function testAddClientEnableSetting()
    {
        $this->formEvent->expects($this->once())
            ->method('getForm')
            ->willReturn($this->form);

        $this->form->expects($this->exactly(2))
            ->method('has')
            ->withConsecutive([Setting::ALIAS_CLIENT_ENABLE], [Setting::ALIAS_CLIENT_ENDPOINT_URL])
            ->willReturnOnConsecutiveCalls(false, true);

        $this->form->expects($this->once())
            ->method('add')
            ->with(Setting::ALIAS_CLIENT_ENABLE, SettingType::class);

        $listener = new AddSettingsListener();
        $listener->onPreSetData($this->formEvent);
    }
}

<?php

namespace Tests\ApiClientBundle;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class RequestTestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $mockData;

    /**
     * @var array
     */
    protected $container = [];

    protected $provider;

    /**
     * @param string $name
     * @param int $containerNo
     *
     * @return mixed
     */
    protected function getQueryParameter($name, $containerNo = 0)
    {
        /** @var Request $request */
        $request = $this->container[$containerNo]['request'];

        parse_str($request->getUri()->getQuery(), $params);

        if (array_key_exists($name, $params)) {
            return $params[$name];
        }

        return null;
    }

    /**
     * @param int $containerNo
     *
     * @return int
     */
    protected function getStatusCode($containerNo = 0): int
    {
        return $this->container[$containerNo]['response']->getStatusCode();
    }
}

<?php

namespace Tests\ApiBundle;

use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class ApiTestCase extends WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ReferenceRepository
     */
    protected $fixtures;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var PropertyAccessor
     */
    private $accessor;

    /**
     * @var array
     */
    private $headers;

    protected function setUp()
    {
        $this->client = $this->makeClient();
    }

    /**
     * @param string $username
     * @param string $password
     */
    protected function setBasicAuthLogin($username, $password)
    {
        $this->headers = [
            'PHP_AUTH_USER' => $username,
            'PHP_AUTH_PW' => $password,
        ];
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $payload
     */
    protected function makeApiRequest($method, $url, array $payload = null)
    {
        $headers = [
            'HTTP_ACCEPT' => 'application/json',
            'CONTENT_TYPE' => 'application/json',
        ];

        if ($this->headers) {
            $headers = array_merge($this->headers, $headers);
        }

        if ($payload) {
            $payload = json_encode(array_merge($payload, ['_format' => 'json']));
        }

        $this->client->request($method, $url, [], [], $headers, $payload);
        $this->response = $this->client->getResponse();
    }

    /**
     * @param Response $response
     * @param string $propertyPath
     */
    protected function assertResponsePropertyExists(Response $response, $propertyPath)
    {
        $this->readResponseProperty($response, $propertyPath);
    }

    /**
     * @param Response $response
     * @param string $propertyPath
     */
    protected function assertResponsePropertyIsArray(Response $response, $propertyPath)
    {
        $this->assertInternalType('array', $this->readResponseProperty($response, $propertyPath));
    }

    /**
     * @param Response $response
     * @param string $propertyPath
     */
    protected function assertResponsePropertyIsObject(Response $response, $propertyPath)
    {
        $this->assertInternalType('object', $this->readResponseProperty($response, $propertyPath));
    }

    /**
     * @param Response $response
     * @param string $propertyPath
     * @param int $expectedCount
     */
    protected function assertResponsePropertyCount(Response $response, $propertyPath, $expectedCount)
    {
        $this->assertCount((int)$expectedCount, $this->readResponseProperty($response, $propertyPath));
    }

    /**
     * @param Response $response
     * @param string $propertyPath
     * @param mixed $expectedValue
     */
    protected function assertResponsePropertyEquals(Response $response, $propertyPath, $expectedValue)
    {
        $actual = $this->readResponseProperty($response, $propertyPath);
        $this->assertEquals(
            $expectedValue,
            $actual,
            sprintf(
                'Property "%s": Expected "%s" but response was "%s"',
                $propertyPath,
                $expectedValue,
                var_export($actual, true)
            )
        );
    }

    /**
     * @param Response $response
     * @param string $propertyPath
     * @param mixed $givenValue
     */
    protected function assertResponsePropertyNotEquals(Response $response, $propertyPath, $givenValue)
    {
        $actual = $this->readResponseProperty($response, $propertyPath);
        $message = sprintf('Property "%s": Not expected "%s" but response was exactly "%s"',
            $propertyPath, $givenValue, var_export($actual, true));
        $this->assertNotEquals($givenValue, $actual, $message);
    }

    /**
     * @param Response $response
     */
    protected function assertResponseHasPagination(Response $response)
    {
        $this->assertResponsePropertyIsArray($response, 'items');
        $this->assertResponsePropertyExists($response, 'total');
        $this->assertResponsePropertyExists($response, 'current');
    }

    /**
     * @param Response $response
     * @param string $propertyPath
     * @return mixed
     */
    private function readResponseProperty(Response $response, $propertyPath)
    {
        if ($this->accessor === null) {
            $this->accessor = PropertyAccess::createPropertyAccessor();
        }

        $data = json_decode((string)$response->getContent());

        try {
            return $this->accessor->getValue($data, $propertyPath);
        } catch (AccessException $e) {
            // it could be a stdClass or an array of stdClass
            $values = is_array($data) ? $data : get_object_vars($data);

            throw new AccessException(sprintf(
                'Error reading property "%s" from available keys (%s)',
                $propertyPath,
                implode(', ', array_keys($values))
            ), 0, $e);
        }
    }
}

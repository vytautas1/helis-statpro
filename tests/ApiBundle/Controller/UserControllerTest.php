<?php

namespace Tests\ApiBundle\Controller;

use AppBundle\DataFixtures\ORM\LoadUserData;
use AppBundle\Entity\User;
use Tests\ApiBundle\ApiTestCase;

class UserControllerTest extends ApiTestCase
{
    private static $createBody = [
        'user' => [
            'email' => 'scorpion@mortal-kombat.com',
            'username' => 'Scoprion',
            'password' => 'getOverHere',
        ]
    ];

    private static $updateBody = [
        'user' => [
            'username' => 'SubZero',
        ],
    ];

    /**
     * @var User
     */
    private $user;

    /**
     * @var User
     */
    protected $admin;

    public function setUp()
    {
        parent::setUp();

        $this->fixtures = $this->loadFixtures([
            'AppBundle\DataFixtures\ORM\LoadUserData',
        ])->getReferenceRepository();

        $this->user = $this->fixtures->getReference('user-mario');
        $this->admin = $this->fixtures->getReference('user-luigi');
    }

    public function testBasicAuthorizationForApi()
    {
        $this->makeApiRequest('GET', '/api/users');

        $this->assertStatusCode(401, $this->client);
    }

    public function testListUsersAccountsInformation()
    {
        $this->setBasicAuthLogin($this->user->getEmail(), LoadUserData::PASSWORD);
        $this->makeApiRequest('GET', '/api/users');

        $this->isSuccessful($this->response, true, 'application/json');
        $this->assertResponsePropertyCount($this->response, 'users', 2);
    }

    public function testGetMyAccountInformation()
    {
        $this->setBasicAuthLogin($this->user->getEmail(), LoadUserData::PASSWORD);
        $this->makeApiRequest('GET', '/api/users/me');

        $this->isSuccessful($this->response, true, 'application/json');
        $this->assertResponsePropertyEquals($this->response, 'email', $this->user->getEmail());
    }

    public function testGetUserAccountInformation()
    {
        $this->setBasicAuthLogin($this->user->getEmail(), LoadUserData::PASSWORD);
        $this->makeApiRequest('GET', '/api/users/' . $this->admin->getId());

        $this->isSuccessful($this->response, true, 'application/json');
        $this->assertResponsePropertyEquals($this->response, 'email', $this->admin->getEmail());
    }

    public function testRegularUserCanNotCreateNewUser()
    {
        $this->setBasicAuthLogin($this->user->getEmail(), LoadUserData::PASSWORD);
        $this->makeApiRequest('POST', '/api/users', self::$createBody);

        $this->assertStatusCode(403, $this->client);
    }

    public function testAdminUserCanCreateUser()
    {
        $this->setBasicAuthLogin($this->admin->getEmail(), LoadUserData::PASSWORD);
        $this->makeApiRequest('POST', '/api/users', self::$createBody);

        $this->isSuccessful($this->response, true, 'application/json');
        $this->assertNotEmpty($this->response->headers->get('Location'));
    }

    public function testRegularUserCanNotUpdateUser()
    {
        $this->setBasicAuthLogin($this->user->getEmail(), LoadUserData::PASSWORD);
        $this->makeApiRequest('PATCH', '/api/users/' . $this->admin->getId(), self::$updateBody);

        $this->assertStatusCode(403, $this->client);
    }

    public function testAdminCanUpdateUser()
    {
        $this->setBasicAuthLogin($this->admin->getEmail(), LoadUserData::PASSWORD);
        $this->makeApiRequest('PATCH', '/api/users/' . $this->user->getId(), self::$updateBody);

        $this->isSuccessful($this->response, true, 'application/json');
    }

    public function testRegularUserCanNotDeleteUser()
    {
        $this->setBasicAuthLogin($this->user->getEmail(), LoadUserData::PASSWORD);
        $this->makeApiRequest('DELETE', '/api/users/' . $this->admin->getId());

        $this->assertStatusCode(403, $this->client);
    }

    public function testAdminCanDeleteUser()
    {
        $this->setBasicAuthLogin($this->admin->getEmail(), LoadUserData::PASSWORD);
        $this->makeApiRequest('DELETE', '/api/users/' . $this->user->getId());

        $this->isSuccessful($this->response, true, 'application/json');
    }
}

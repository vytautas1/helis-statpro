#!/bin/bash

echo "[info] Starting docker"
cp docker-compose.yml.dist docker-compose.yml
cp .env.dist .env
docker-compose up --build

#!/bin/bash

echo "[info] Changing permissions for storage/"
chmod -R 777 /var/www/var/cache /var/www/var/logs /var/www/var/sessions

echo "[info] Running composer"
/usr/bin/composer self-update
composer install --optimize-autoloader --working-dir=/var/www

echo "[info] Clearing the cache"
php /var/www/bin/console cache:clear --no-warmup

echo "[info] Waiting for mysql"
sleep 10

echo "[info] Migrating database"
php /var/www/bin/console doctrine:migrations:migrate --no-interaction

echo "[info] Applying fixtures"
php /var/www/bin/console doctrine:fixtures:load --no-interaction

echo "[info] Warming the cache"
php /var/www/bin/console cache:warmup

echo "[info] Applying ownership to www user"
chown -R www:www /var/www/var/cache /var/www/var/logs /var/www/var/sessions /var/www/vendor

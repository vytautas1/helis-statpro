# eSports StatPro™

Simple web application with API client and exposed API

## Prerequisites

To get environment up and running, you need to install **Docker Engine** and **Docker Compose** first.

Docker Engine and Docker Compose installation guides can found here: https://docs.docker.com/compose/

## Installation & Setup

Before starting Docker containers first we need to create Docker network by running this command:
 
```
docker network create general
```
 
The only command which is needed for booting up this application:

```
$ bin/docker-start
```

By default, when containers starts up, application can be accessed on http://localhost:8080

Furthermore, using automated nginx proxy for Docker, this application can be accessed
via virtual host, defined in **.env.yml** file. By defaults its http://statpro.dev

If somewhat application can't be accessed via virtual host, then probably you need to add hostname
your system hosts file and point it to localhost:

```
127.0.0.1    statpro.dev
```

## Application

### User

By default there are two users loaded already to the system via fixtures:

| Role         | Email             | Password       |
| ------------ | ----------------- | ---------------|
| Default user | mario@statpro.dev | S3cretpassword |
| Admin user   | luigi@statpro.dev | S3cretpassword |

To access to the system you can use any of these credentials. But if you need to create new user (especially with admin role)
you can do it by using Symfony console command:

```
bin/console app:user:create
```

User password can be also changed via another Symfony console command:

```
bin/console app:user:change-password
```

### API Client Settings

Currently there are two settings implemented for API Client:
* to enable/disable client
* to specify API endpoint URL

By default these settings are taken from **.env.yml.dist** file. If settings are saved using Settings page, then they
will take precedence over values from configuration file.

### Exposed API

Application also contains API itself, which currently exposes only User model.

All existing API endpoints are well documented
using Swagger and can be located http://statpro.dev/swagger/index.html

## Running the tests

Project contains **phpunit** tests, which can be executed by running command:

```
$ vendor/bin/phpunit
```

## Built With

* [Symfony3](http://symfony.com/doc/current/index.html) - The framework used
* [Docker](https://docs.docker.com/) - Software container platform 
